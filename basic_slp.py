import numpy as np
from netCDF4 import Dataset
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import cartopy.crs as crs
from cartopy.feature import NaturalEarthFeature

from wrf import (to_np, getvar, smooth2d, get_cartopy, cartopy_xlim,
                 cartopy_ylim, latlon_coords)

# Open the NetCDF file
ncfile = Dataset("wrfout_d03_2019-09-10_newfirererun.nc")

# Get the sea level pressure
slp = getvar(ncfile, "slp" , timeidx=0)

cart_proj = get_cartopy(slp)
lats, lons = latlon_coords(slp)

fig = plt.figure(figsize=(10,10))
ax = plt.axes(projection=cart_proj)

ax.coastlines(linewidth=0.5)

lvl = np.arange(990, 1030, 2.5)

plt.contour(lons,
            lats,
            slp,
            level=lvl,
            colors='k',
            transform=crs.PlateCarree())

plt.contourf(lons,
            lats,
            slp,
            level=lvl,
            transform=crs.PlateCarree(),
            cmap='viridis')

plt.colorbar(ax=ax, shrink=0.7)

plt.show()
