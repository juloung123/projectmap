from netCDF4 import Dataset
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import cartopy.crs as crs
from cartopy.feature import NaturalEarthFeature

from wrf import (to_np, getvar, smooth2d, get_cartopy, cartopy_xlim,
                 cartopy_ylim, latlon_coords)

# Open the NetCDF file
ncfile = Dataset("wrfout_d03_2019-09-10_newfirererun.nc")

# Get the sea level pressure
PM2_5_DRY = getvar(ncfile, "PM2_5_DRY")[0,:]

# Smooth the sea level pressure since it tends to be noisy near the
# mountains
smooth_PM2_5_DRY = smooth2d(PM2_5_DRY, 3, cenweight=4)

# Get the latitude and longitude points
lats, lons = latlon_coords(PM2_5_DRY)

# Get the cartopy mapping object
cart_proj = get_cartopy(PM2_5_DRY)

# Create a figure
fig = plt.figure(figsize=(12,6))
# Set the GeoAxes to the projection used by WRF
ax = plt.axes(projection=cart_proj)

# Download and add the states and coastlines
states = NaturalEarthFeature(category="cultural", scale="50m",
                             facecolor="none",
                             name="admin_1_states_provinces_shp")
ax.add_feature(states, linewidth=.5, edgecolor="black")
ax.coastlines('50m', linewidth=0.8)

# Make the contour outlines and filled contours for the smoothed sea level
# pressure.
plt.contour(to_np(lons), to_np(lats), to_np(smooth_PM2_5_DRY), 10, colors="black",
            transform=crs.PlateCarree())
plt.contourf(to_np(lons), to_np(lats), to_np(smooth_PM2_5_DRY), 10,
             transform=crs.PlateCarree(),
             cmap=get_cmap("jet"))

# Add a color bar
plt.colorbar(ax=ax, shrink=.98)

# Set the map bounds
ax.set_xlim(cartopy_xlim(smooth_PM2_5_DRY))
ax.set_ylim(cartopy_ylim(smooth_PM2_5_DRY))

# Add the gridlines
ax.gridlines(color="black", linestyle="dotted")

plt.title("Sea Level Pressure (hPa)")

plt.show()

# plt.savefig('/home/julong/NetCDF/test.jpg')
# plt.clf()