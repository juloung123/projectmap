import numpy as np
from netCDF4 import Dataset
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import matplotlib as mpl
import cartopy.crs as crs
from cartopy.feature import NaturalEarthFeature
from matplotlib.colors import LinearSegmentedColormap
from wrf import (to_np, getvar, smooth2d, get_cartopy, cartopy_xlim,
                 cartopy_ylim, latlon_coords)

# Open the NetCDF file
ncfile = Dataset("wrfout_d03_2019-09-10_newfirererun.nc")
#ncfile = Dataset("wrfout")


############### map #################

# cdict = {
#    'red'  :  ( (0.0, 0.25, .25), (0.02, .59, .59), (1., 1., 1.)),
#    'green':  ( (0.0, 0.0, 0.0), (0.02, .45, .45), (1., .97, .97)),
#    'blue' :  ( (0.0, 1.0, 1.0), (0.02, .75, .75), (1., 0.45, 0.45))
# }

cm = mpl.colors.LinearSegmentedColormap.from_list('my_colormap', ['#00c7ff' , '#6ee44b', '#f1ff00', '#ffa500', '#ff0024'], 1024)
#cm = mpl.colors.LinearSegmentedColormap.from_list('my_colormap', ['#4CC9F0' , '#BC3908'], 1024)
############### map #################

s_25 = 0.942
d_25 = 0.286
days = np.arange(0,337)
#days = np.arange(0,1)

for i in days:
    # Get the sea level pressure
    PM2_5_DRY = getvar(ncfile, "PM2_5_DRY", timeidx=i)[0,:]
    DUST_1 = getvar(ncfile, "DUST_1", timeidx=i)[0,:]
    DUST_2 = getvar(ncfile, "DUST_2", timeidx=i)[0,:]
    SEAS_1 = getvar(ncfile, "SEAS_1", timeidx=i)[0,:]
    SEAS_2 = getvar(ncfile, "SEAS_2", timeidx=i)[0,:]
    #################################
    #PM2_5 = PM2_5_DRY * (DUST_1 + DUST_2 * d_25 + SEAS_1 + SEAS_2 * s_25)
    PM2_5 = PM2_5_DRY
    #################################
    #print(type(PM2_5_DRY))
    #PM2_5_DRY = getvar(ncfile, "DUST_3", timeidx=i)[0,:]

    #cart_proj = get_cartopy(PM2_5_DRY)
    #lats, lons = latlon_coords(PM2_5)

    cart_proj = get_cartopy(PM2_5)
    lats, lons = latlon_coords(PM2_5)


    fig = plt.figure(figsize=(19,12))


    ax = plt.axes(projection=crs.PlateCarree())
    #ax.set_global()
    ax.set_extent([90, 110 , 0, 20])
    ax.stock_img()
    ax.coastlines(linewidth=0.5)

    lvl = np.arange(990, 1030, 2.5)

    # plt.contour(lons,
    #             lats,
    #             PM2_5,
    #             level=lvl,
    #             colors='k',
    #             transform=crs.PlateCarree())

    plt.contourf(lons,
                lats,
                PM2_5,
                # level=lvl,
                transform=crs.PlateCarree(),
                #cmap=plt.get_cmap('jet'))
                cmap=cm)
    #################################
    #convert numpy.datetime64 to string
    t = np.datetime64(PM2_5.Time.values)
    date = np.datetime_as_string(t, unit='D')
    plt.title('PM2.5_DRY : ' + date)

    axs, _ = mpl.colorbar.make_axes(plt.gca(), shrink=0.5)  

    #cbar = mpl.colorbar.ColorbarBase(axs, cmap=plt.get_cmap('jet'),
    cbar = mpl.colorbar.ColorbarBase(axs, cmap=cm,
                       norm=mpl.colors.Normalize(vmin=-0, vmax=100))
    #cbar.set_clim(-2.0, 2.0)

    #plt.pcolor(PM2_5, cmap='jet', vmin= -50, vmax=10)
    ################################
    
    #plt.colorbar(ax=ax, shrink=0.7)

    plt.savefig('/home/julong/NetCDF/pic_out/'+ str(i) +'.jpg')
    #plt.show()




