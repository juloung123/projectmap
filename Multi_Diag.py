import numpy as np
from netCDF4 import Dataset
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import cartopy.crs as crs
from cartopy.feature import NaturalEarthFeature

from wrf import (to_np, getvar, smooth2d, get_cartopy, cartopy_xlim,
                 cartopy_ylim, latlon_coords)

# Open the NetCDF file
ncfile = Dataset("wrfout_d03_2019-09-10_newfirererun.nc")

# Use single wrf file for this example
# Get "PM2_5_DRY", "td2", "ua", and "va" variables 
PM2_5_DRY = getvar(ncfile, "PM2_5_DRY", timeidx=0)[0,:]
td2 = getvar(ncfile, "td2", timeidx=0, units="degF") # Use units "degF"
u_sfc = getvar(ncfile, "ua", timeidx=0, units="kts")[0,:] # subset data to match size of td2 and PM2_5_DRY, use units "kts"
v_sfc = getvar(ncfile, "va", timeidx=0, units="kts")[0,:]

# Get the cartopy object and the lat,lon coords
cart_proj = get_cartopy(PM2_5_DRY)
lats, lons = latlon_coords(PM2_5_DRY)

# Create a figure and get the axes object
fig = plt.figure(figsize=(10, 7.5))
ax = plt.axes(projection=cart_proj)

# Add projection features
ax.coastlines(linewidth=0.5)

# Manually setting the contour levels
PM2_5_DRY_levels = np.arange(980, 1030, 2.5)
td2_levels = np.arange(48, 85, 3)

# Make sea level pressure contour lines.
PM2_5_DRY_contours = plt.contour(lons,
                           lats,
                           PM2_5_DRY,
                           levels=PM2_5_DRY_levels,
                           colors='k',
                           transform=crs.PlateCarree())

# Make filled countours for 2m dewpoint temperature.
plt.contourf(lons,
             lats,
             td2,
             levels=td2_levels,
             transform=crs.PlateCarree(),
             cmap='magma')

# Plot the wind barbs, but only plot ~7 barbs in each direction.
thin = [int(x/7.) for x in lons.shape]
plt.barbs(to_np(lons[::thin[0], ::thin[1]]), # Why are we using to_np here?
          to_np(lats[::thin[0], ::thin[1]]),
          to_np(u_sfc[::thin[0], ::thin[1]]),
          to_np(v_sfc[::thin[0], ::thin[1]]),
          transform=crs.PlateCarree(),
          color = 'green')

# Add contour labels for pressure
plt.clabel(PM2_5_DRY_contours, fmt="%i")

# Add a color bar
plt.colorbar(ax=ax, shrink=.86, extendrect=True)

plt.show()