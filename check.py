from netCDF4 import Dataset
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import cartopy.crs as crs
from cartopy.feature import NaturalEarthFeature
import numpy as np
from wrf import (to_np, getvar, smooth2d, get_cartopy, cartopy_xlim,
                 cartopy_ylim, latlon_coords)


ncfile = Dataset("wrfout_d03_2019-09-10_newfirererun.nc")

s_25 = 0.942
d_25 = 0.286

#DUST_1 = getvar(ncfile, "DUST_1", timeidx=1)[0,:]
DUST_2 = getvar(ncfile, "DUST_2", timeidx=2)[0,:]
#SEAS_1 = getvar(ncfile, "SEAS_1", timeidx=1)[0,:]
#SEAS_2 = getvar(ncfile, "SEAS_2", timeidx=1)[0,:]
#Time = getvar(ncfile, "Times")
#XTime = getvar(ncfile, "XTIME", timeidx=300)
#print(Time)
t = np.datetime64(DUST_2.Time.values)
date = np.datetime_as_string(t, unit='D')
print(date)
# print(SEAS_1)
# print(SEAS_2)


#print(ncfile.variables.keys())
#slp = getvar(ncfile, "ua")[0,:]
#print(slp)
#print('----------------------------------------------------------------------------------------------------------------------------------')
# newDUST= DUST_1 * DUST_2
#print(XTime)
#PM2_5_DRY = getvar(ncfile, "s_25")
#PM2_5_DRY = getvar(ncfile, "PM10")

#print(PM2_5_DRY.shape)
#print(PM2_5_DRY)
#print('-------------------------------------------------------------------------------------------------------------------------------------')
# <xarray.DataArray 'slp' (south_north: 90, west_east: 159)>
# array([[1010.8704 , 1010.8819 , 1010.8945 , ..., 1011.24615, 1011.2315 ,
#         1011.21783],
#        [1010.8717 , 1010.88275, 1010.8941 , ..., 1011.23224, 1011.2186 ,
#         1011.2046 ],
#        [1010.8789 , 1010.89246, 1010.90076, ..., 1011.22046, 1011.2075 ,
#         1011.19324],
#        ...,
#        [1012.4028 , 1012.70013, 1012.9983 , ..., 1011.28876, 1011.2941 ,
#         1011.32745],
#        [1012.17316, 1012.3565 , 1012.55865, ..., 1011.3167 , 1011.3223 ,
#         1011.2986 ],
#        [1012.0792 , 1011.9113 , 1011.9758 , ..., 1011.28674, 1011.3208 ,
#         1011.3254 ]], dtype=float32)
# Coordinates:
#     XLONG    (south_north, west_east) float32 98.52 98.54 98.57 ... 102.8 102.8
#     XLAT     (south_north, west_east) float32 6.029 6.03 6.03 ... 8.463 8.463
#     XTIME    float32 0.0
#     Time     datetime64[ns] 2019-09-10
# Dimensions without coordinates: south_north, west_east
# Attributes:
#     FieldType:    104
#     MemoryOrder:  XY
#     description:  sea level pressure
#     units:        hPa
#     stagger:      
#     coordinates:  XLONG XLAT XTIME
#     projection:   LambertConformal(stand_lon=102.0, moad_cen_lat=4.5000076293...
# ----------------------------------------------------------------------------------------------------------------------------------
# <xarray.DataArray 'PM2_5_DRY' (bottom_top: 32, south_north: 90, west_east: 159)>
# array([[[1.1518713 , 1.1518615 , 1.1518507 , ..., 1.1528668 ,
#          1.1525315 , 1.152227  ],
#         [1.15189   , 1.1518782 , 1.1518652 , ..., 1.1527603 ,
#          1.1524357 , 1.1521268 ],
#         [1.1519006 , 1.1518861 , 1.1518707 , ..., 1.1525894 ,
#          1.152275  , 1.1519723 ],
#         ...,
#         [1.1556733 , 1.158305  , 1.1602015 , ..., 1.149869  ,
#          1.1498566 , 1.1498675 ],
#         [1.1535403 , 1.156417  , 1.158707  , ..., 1.1499078 ,
#          1.1498939 , 1.1498572 ],
#         [1.1503613 , 1.1533043 , 1.155876  , ..., 1.1498995 ,
#          1.1499072 , 1.1498921 ]],

#        [[1.146891  , 1.1468759 , 1.1468593 , ..., 1.1492469 ,
#          1.1488955 , 1.1485678 ],
#         [1.1468992 , 1.1468827 , 1.1468469 , ..., 1.1490625 ,
#          1.1487032 , 1.1483766 ],
#         [1.1468999 , 1.1468773 , 1.146856  , ..., 1.1488096 ,
#          1.1484786 , 1.1481631 ],
# ...
#         [0.11432748, 0.11433133, 0.11433504, ..., 0.11286362,
#          0.11287082, 0.11287819],
#         [0.11432993, 0.11433415, 0.11433803, ..., 0.11288933,
#          0.11289635, 0.11290372],
#         [0.11433239, 0.1143368 , 0.11434101, ..., 0.11291539,
#          0.11292242, 0.11292962]],

#        [[0.09466356, 0.09466285, 0.09466213, ..., 0.09482542,
#          0.09482225, 0.09481893],
#         [0.09466472, 0.09466399, 0.09466327, ..., 0.09482499,
#          0.09482182, 0.09481865],
#         [0.09466486, 0.09466399, 0.09466313, ..., 0.0948247 ,
#          0.09482168, 0.09481865],
#         ...,
#         [0.09480395, 0.09480137, 0.09479848, ..., 0.09445331,
#          0.09444903, 0.09444489],
#         [0.0948018 , 0.09479906, 0.09479618, ..., 0.09445603,
#          0.09445161, 0.09444731],
#         [0.09479949, 0.09479675, 0.09479387, ..., 0.09445889,
#          0.09445418, 0.09444989]]], dtype=float32)
# Coordinates:
#     XLONG    (south_north, west_east) float32 98.52 98.54 98.57 ... 102.8 102.8
#     XLAT     (south_north, west_east) float32 6.029 6.03 6.03 ... 8.463 8.463
#     XTIME    float32 0.0
#     Time     datetime64[ns] 2019-09-10
# Dimensions without coordinates: bottom_top, south_north, west_east
# Attributes:
#     FieldType:    104
#     MemoryOrder:  XYZ
#     description:  pm2.5 aerosol dry mass
#     units:        ug m^-3
#     stagger:      
#     coordinates:  XLONG XLAT XTIME
#     projection:   LambertConformal(stand_lon=102.0, moad_cen_lat=4.5000076293...
