import numpy as np
from netCDF4 import Dataset
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import cartopy.crs as crs
from cartopy.feature import NaturalEarthFeature

from wrf import (to_np, getvar, smooth2d, get_cartopy, cartopy_xlim,
                 cartopy_ylim, latlon_coords)

# Open the NetCDF file
ncfile = Dataset("wrfout_d03_2019-09-10_newfirererun.nc")
#ncfile = Dataset("wrfout")

days = np.arange(0,60)

for i in days:
    # Get the sea level pressure
    PM2_5_DRY = getvar(ncfile, "PM2_5_DRY", timeidx=i)[0,:]
    #PM2_5_DRY = getvar(ncfile, "DUST_3", timeidx=i)[0,:]

    cart_proj = get_cartopy(PM2_5_DRY)
    lats, lons = latlon_coords(PM2_5_DRY)

    fig = plt.figure(figsize=(19,12))
    ax = plt.axes(projection=cart_proj)

    ax.coastlines(linewidth=0.5)

    lvl = np.arange(990, 1030, 2.5)

    # plt.contour(lons,
    #             lats,
    #             PM2_5_DRY,
    #             level=lvl,
    #             colors='k',
    #             transform=crs.PlateCarree())

    plt.contourf(lons,
                lats,
                PM2_5_DRY,
                # level=lvl,
                transform=crs.PlateCarree(),
                cmap='jet')

    plt.colorbar(ax=ax, shrink=0.7)

    #ax.set_extent([-120, -80 , 20, 28])
    plt.title('testing PM2.5 DRY : ' + str(i))
    plt.savefig('/home/julong/NetCDF/pic_out/'+ str(i) +'.jpg')
    #plt.show()




