from PIL import Image, ImageDraw
import numpy as np 

image_frames=[]

days = np.arange(0,60)

for k in days:
    new_frame = Image.open('/home/julong/NetCDF/pic_out/'+ str(k) +'.jpg')
    image_frames.append(new_frame)

image_frames[0].save('/home/julong/NetCDF/gif_out/test.gif', format = 'GIF', append_images = image_frames[1: ], save_all = True, duration = 300, loop = 0) 